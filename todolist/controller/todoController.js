render = (item) => {
    var contentHTML="";
    item.forEach((data) => {
        let {id, mota, tiendo} = data;
        var contentTr =`
        <li class="item-card col-sm-6 col-lg-4 mt-lg-0">
        <div class="moTa">${mota}</div>
        <button onclick="remove(${id})" class="btn btn-primary shop-item-button w-50 p-3" ><i class="fa fa-trash-alt"></i></button>
        <input onChange="markComplete(${id})" type="checkbox" ${tiendo ? "checked" : ""}>
        </li>`;
        contentHTML += contentTr
    });
    return document.getElementById("todo").innerHTML = contentHTML;
    ;
};

const renderListTask = function (itemTask){
    const viecchuaxong = itemTask.filter((data) => {
        return data.tiendo === false;
    });
    const viecdaxong = itemTask.filter((data) => {
        return data.tiendo === true;
    });
    document.getElementById("completed").innerHTML = render(viecdaxong);
    document.getElementById("todo").innerHTML = render(viecchuaxong);
}
