const BASE_URL = "https://636652cd046eddf1bafd7a48.mockapi.io/";

function showAllData(){
    axios({
        url: `${BASE_URL}/Todolist`,
        method: "GET",
    })
    .then(function(res){
        renderListTask(res.data);
        setLocalStorage(res.data);
    })
    .catch(function(err){
        console.log("err:",err);
    });
}
showAllData();

const buttonAddTask = function () {
    const input = document.getElementById("newTask").value;
  
    if (input) {
      const mota = new Task(input);
  
      axios({
        url: `${BASE_URL}/Todolist`,
        method: "POST",
        data: mota,
      })
        .then((res) => {
            resetInput();
            showAllData();
          resetPage();
          
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };


const resetInput = () => {
    document.getElementById("newTask").value = ""
}

const remove = function (id) {
    axios({
      url: `${BASE_URL}/Todolist/${id}`,
      method: "DELETE",
    })
    .then(function(res){
        showAllData();
        resetPage();
      })
      .catch((err) => {
        console.log(err);
      });
};

const markComplete = function (id) {
    let obj = null;
  
    axios({
      url: `${BASE_URL}/Todolist/${id}`,
      method: "GET",
    })
      .then((res) => {
        obj = res.data;
        obj.tiendo = !obj.tiendo;
        markedUpdate(obj);
      })
      .catch((err) => {
        console.log(err);
      });
};

const markedUpdate = function (obj) {
    axios({
      url: `${BASE_URL}/Todolist/${obj.id}`,
      method: "PUT",
      data: obj,
    })
      .then((res) => {
        showAllData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const sortAtoZ = () => {
      axios({
        url: `${BASE_URL}/Todolist`,
        method: "GET",
      })
      .then((res) => {
        const sortedTasksAtoZ = res.data.sort((item1, item2) => {
            const motaA = item1.mota.toString().toLowerCase();
            console.log("motaA",motaA)
            const motaB = item2.mota.toString().toLowerCase();
            console.log("motaB",motaB)
          if (motaA < motaB) {
            return -1;
          }
          if (motaA > motaB) {
            return 1;
          }
          return 0;
        });
        renderListTask(sortedTasksAtoZ);
      })
      .catch((err) => {
        console.log(err);
      });
  };


const sortZtoA = () => {
    axios({
      url: `${BASE_URL}/Todolist`,
      method: "GET",
    })
    .then((res) => {
      const sortedTasksZtoA = res.data.sort((item1, item2) => {
          const motaA = item1.mota.toString().toLowerCase();
          console.log("motaA",motaA)
          const motaB = item2.mota.toString().toLowerCase();
          console.log("motaB",motaB)
        if (motaA > motaB) {
          return -1;
        }
        if (motaA < motaB) {
          return 1;
        }
        return 0;
      });
      renderListTask(sortedTasksZtoA);
    })
    .catch((err) => {
      console.log(err);
    });
};



function setLocalStorage(luuToDo){
    localStorage.setItem("TodoList",JSON.stringify(luuToDo));
}
function getLocalStorage(traKetQua){
    var traKetQua = JSON.parse(localStorage.getItem("TodoList"));
    return traKetQua
}
const resetPage = () => {
    window.location.reload();
}